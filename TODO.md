# Ideas of improvements

## Opening XML files

- Not important: monitor XML files to detect modifications
- Not important: ability to open an XML file by drag-and-dropping it from nautilus

## Adding and managing pictures

- Very important: **ensure the memory is freed when deleting a row**
- Important: less ridiculous "automatic fix"
- Not important: ability to add a picture by drag-and-dropping the file from nautilus
- Not important: search entry

## General

- Important: don't save the `/run/user/1000/doc/...` path in dconf???
- Not important: more clear and useful error messages, including line numbers

## Translations

- Important: provide translations to the description in `data/com.github.maoschanz.DynamicWallpaperEditor.appdata.xml.in`, but without cluttering the `.po` file with release notes bullshit
- Not important: Provide translations for the help user manual

